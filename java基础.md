# 1. map
## HashMap和HashTable区别？
#### 相同点；
	- HashMap和HashTable都可以存储key-value数据
        - 且两者的的key值均不能重复，若添加key相同的键值对，后面的value会自动覆盖前面的value，但不会报错。
#### 区别：
	- HashMap: key/value 都可以为null
	- HashTable: key/value 都不可以为null。
```java
	Map<String ,String> hashmap = new HashMap<String,String>();
        hashmap.put(null,null);
        Map<String,String> hashtable = new Hashtable<>();
        hashtable.put("1","111");
```
	
